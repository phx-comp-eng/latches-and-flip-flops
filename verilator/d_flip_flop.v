module d_flip_flop(
    pre,
    clr,
    clk,
    d,
    q
);

input pre;
input clr;
input clk;
input d;
output reg q;

always @(posedge pre or posedge clr or posedge clk) begin
    if (pre == 1'b1) begin
        q <= 1;
    end else if (clr == 1'b1) begin
        q <= 0;
    end else if (clk == 1'b1) begin
        q <= d;
    end else begin
        q <= d;
    end
end

endmodule
