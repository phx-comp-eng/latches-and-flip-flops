#include <stdlib.h>
#include "Vd_flip_flop.h"
#include "verilated.h"
#include "verilated_vcd_c.h"

int main(int argc, char **argv) {
    bool res = true;
    uint16_t time = 0;

    // Initialize Verilators variables
    Verilated::commandArgs(argc, argv);

    // Create an instance of our module under test
    Vd_flip_flop *tb = new Vd_flip_flop;

    // Configure VCD tracing.
    Verilated::traceEverOn(true);
    VerilatedVcdC* tfp = new VerilatedVcdC;

    tb->trace(tfp, 99);
    tfp->open("test_d_flip_flop.vcd");

    // Begin tests.

    tb->eval();
    tfp->dump(time);

    time += 1;

    tb->pre = 0b1;
    tb->eval();
    tfp->dump(time);

    if (tb->q != 0b1) {
        printf("Test pre failed.\n");
        res = false;
    }

    time += 1;

    tb->pre = 0b0;
    tb->eval();
    tfp->dump(time);

    if (tb->q != 0b1) {
        printf("Test pre failed.\n");
        res = false;
    }

    time += 1;

    tb->clr = 0b1;
    tb->eval();
    tfp->dump(time);

    if (tb->q != 0b0) {
        printf("Test clr failed.\n");
        res = false;
    }

    time += 1;

    tb->clr = 0b0;
    tb->clk = 0b1;
    tb->eval();
    tfp->dump(time);

    if (tb->q != 0b0) {
        printf("Test clr failed.\n");
        res = false;
    }

    time += 1;

    tb->d = 0b1;
    tb->eval();
    tfp->dump(time);

    if (tb->q != 0b0) {
        printf("Test edge trigger failed.\n");
        res = false;
    }

    time += 1;

    tb->eval();
    tfp->dump(time);

    tb->clk = 0b0;
    tb->eval();
    tfp->dump(time);

    time += 1;

    tb->eval();
    tfp->dump(time);

    time += 1;

    tb->eval();
    tfp->dump(time);

    tb->clk = 0b1;
    tb->eval();
    tfp->dump(time);

    if (tb->q != 0b1) {
        printf("Test edge trigger failed.\n");
        res = false;
    }

    time += 1;

    tb->eval();
    tfp->dump(time);

    time += 1;

    tb->clk = 0b0;
    tb->eval();
    tfp->dump(time);

    time += 1;

    tb->eval();
    tfp->dump(time);

    if (!res) {
        printf("Tests failed.\n");
    } else {
        printf("Tests passed.\n");
    }

    tfp->close();

	exit(EXIT_SUCCESS);
}
