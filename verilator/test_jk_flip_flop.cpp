#include <stdlib.h>
#include "Vjk_flip_flop.h"
#include "verilated.h"
#include "verilated_vcd_c.h"

int main(int argc, char **argv) {
    bool res = true;
    uint16_t time = 0;

    // Initialize Verilators variables
    Verilated::commandArgs(argc, argv);

    // Create an instance of our module under test
    Vjk_flip_flop *tb = new Vjk_flip_flop;

    // Configure VCD tracing.
    Verilated::traceEverOn(true);
    VerilatedVcdC* tfp = new VerilatedVcdC;

    tb->trace(tfp, 99);
    tfp->open("test_jk_flip_flop.vcd");

    // Begin tests.

    tb->eval();
    tfp->dump(time);

    time += 1;

    tb->j = 0b1;
    tb->eval();
    tfp->dump(time);

    time += 1;

    tb->clk = 0b1;
    tb->eval();
    tfp->dump(time);

    if (tb->q != 0b1) {
        printf("Test high j failed.\n");
        res = false;
    }

    time += 1;

    tb->eval();
    tfp->dump(time);

    time += 1;

    tb->clk = 0b0;
    tb->eval();
    tfp->dump(time);

    time += 1;

    tb->j = 0b0;
    tb->k = 0b1;
    tb->eval();
    tfp->dump(time);

    time += 1;

    tb->clk = 0b1;
    tb->eval();
    tfp->dump(time);

    if (tb->q != 0b0) {
        printf("Test high k failed.\n");
        res = false;
    }

    time += 1;

    tb->eval();
    tfp->dump(time);

    time += 1;

    tb->clk = 0b0;
    tb->eval();
    tfp->dump(time);

    time += 1;

    tb->j = 1;
    tb->k = 1;
    tb->eval();
    tfp->dump(time);

    time += 1;

    tb->clk = 0b1;
    tb->eval();
    tfp->dump(time);

    if (tb->q != 0b1) {
        printf("Test high jk, low q failed.\n");
        res = false;
    }

    time += 1;

    tb->eval();
    tfp->dump(time);

    time += 1;

    tb->clk = 0b0;
    tb->eval();
    tfp->dump(time);

    time += 1;

    tb->eval();
    tfp->dump(time);

    time += 1;

    tb->clk = 0b1;
    tb->eval();
    tfp->dump(time);

    if (tb->q != 0b0) {
        printf("Test high jk, high q failed.\n");
        res = false;
    }

    time += 1;

    tb->eval();
    tfp->dump(time);

    time += 1;

    tb->eval();
    tfp->dump(time);

    time += 1;

    tb->clk = 0b0;
    tb->eval();
    tfp->dump(time);

    if (!res) {
        printf("Tests failed.\n");
    } else {
        printf("Tests passed.\n");
    }

    tfp->close();

	exit(EXIT_SUCCESS);
}
