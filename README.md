Overview
========

*Latches and Flip-flops* is a talk prepared for and given
at the 2019.2 session of the Phoenix Computer Engineering Club. We continue with
the format of the previous talk. First, we introduce latches and flip-flops
(specific types of memory elements). Then we build some of these circuits
in Verilog and simulate and visualize them with Verilator and GTKWave.

Dependencies
============

* GNU Make, GCC, and Verilator to build the demonstration.
* GTKWave to view the VCD output files from the demonstration.
* LibreOffice Impress, if editing the presentation is desired.

Build and Execute
=================

```
cd verilator
make
./obj_dir/Vd_flip_flop
./obj_dir/Vjk_flip_flop
```

Copyright and License
=====================

This talk and all associated materials are free (libre).

Copyright 2019 Francis (Fritz) Mahnke

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
